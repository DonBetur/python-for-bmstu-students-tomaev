# Курс численное моделирование динамики КА на Python
Поступать на курсы Stepik необходимо по приведённым здесь ссылкам, иначе не будет
работать автоматический учёт посещений!

Если у Вас возникают проблемы с регистрацией в Gitlab из РФ, то у этого репозитория есть
зеркало на российском хостинге
[Gitflic](https://gitflic.ru/project/zhumaev/python_for_bmstu_students). Все действия
можно аналогично выполнять там. Автор репозитория будет периодически синхронизировать
изменения между репозиториями.

## 1. Условия получения зачёта
Для получения зачёта необходимо выполнить все условия:
- Набрать 20 баллов, посмотреть баллы можно
  [здесь](https://docs.google.com/spreadsheets/d/1Jmev3Km2hTD-y_fOnPQvEdnbQF67wohk/edit#gid=856753967)
- Решить все задания [курса по численному моделированию динамики КА на
  Python](https://stepik.org/join-class/e2642bf8c29fb959aa2576643f8bbcbcf841c4ac) на
  Stepik.
- В файл [lessons/lesson_02/print_students.py](lessons/lesson_02/print_students.py)
  необходимо внести свои ФИО в уже созданной ветке "print_students" и отправить
  изменения ("запушить" от англ. push) на сервер
- Выполнить обязательное домашнее задание (см. ниже).
- Сдать рубежный контроль (можно сдать "автоматом", см. ниже).

## 2. Как можно набрать баллы
### 2.1 Выполнение заданий с лекций/семинаров:
- задания со всех лекций вовремя: 10 баллов
- 13 заданий вовремя: 8 баллов
- 10 заданий вовремя: 5 баллов
- иначе: 0

Посещения учитываются по выполнению заданий на Stepik.

### 2.2 Обязательное домашнее задание:
1..10 баллов - зависит от задачи, список задач оформлен в виде [issues](https://gitlab.com/Zaynulla/python_for_bmstu_students/-/issues) этого проекта.

Можно решить любую задачу, которую ещё не решили другие студенты.
Перед тем как начать делать задачу необходимо "забронировать", т.е. поменять поле assignee и указать себя.

### 2.3 Прохождение дополнительных курсов (по желанию или если не хватает баллов)
- Выполнение всех заданий базового курса по Python ([ссылка](https://stepik.org/join-class/bb6c873b6f61656ebe06f142abb194b908add244)) - 4 балла

- Набрать >=1500 баллов продвинутого курса по Python ([ссылка](https://stepik.org/join-class/111e2ef8a11d405cde91676c434aabeadd43de59)) - 9 баллов

- Набрать >=70 баллов курса по математике на Python ([ссылка](https://stepik.org/join-class/df31701fa197a5f77541bcbc5afbb761af9cc4f4)) - 9 баллов

## 3. Рубежный контроль
- "Автоматом" для сдавших ДЗ до 12-й недели включительно. ДЗ сдано если решение задачи
  принято преподавателем.
- Для остальных: очный экзамен с теорией языка Python и решением задач по
  программированию на Python.
