import numpy as np
import pytest
from magnetic import B_dot_body

B_dot_body_test_data = [
    pytest.param([1, 0, 0], [1, 0, 0], [0, 0, 0], id="x rotation"),
    pytest.param([1, 0, 0], [0, 1, 0], [0, 0, 1], id="y rotation"),
    pytest.param([1, 0, 0], [0, 0, 1], [0, -1, 0], id="z rotation"),
    pytest.param([1, 0, 0], [5, 8, 1], [0, -1, 8], id="mixed rotation"),
    pytest.param([1, 0, 0], [0, 0, 0], [0, 0, 0], id="no rotation"),
]


@pytest.mark.parametrize(
    "magnetic_vector, sat_omega_body, expected_b_dot_vector", B_dot_body_test_data
)
def test_B_dot_body(magnetic_vector, sat_omega_body, expected_b_dot_vector):
    # все компоненты посчитанного вектора должны совпадать с соответствующими
    # компонентами ожидаемого вектора
    assert (
        B_dot_body(magnetic_vector, sat_omega_body) == np.array(expected_b_dot_vector)
    ).all()
