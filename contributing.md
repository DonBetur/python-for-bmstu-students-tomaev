# Требования к внесению изменений в код
Перед выполнением большинства команд git желательно актуализировать локальное состояние
с облаком, чтобы потом не возникали конфликты слияния:
```bash
git pull
```

## 1. Использовать feature branch workflow
Для каждой задачи необходимо создавать отдельную ветку, что также называется [feature
branch workflow](https://docs.gitlab.com/ee/gitlab-basics/feature_branch_workflow.html)

Например, для [issue 35](https://gitlab.com/Zaynulla/python_for_bmstu_students/-/issues/35)
соответствующую ветку с именем feature/35 можно создать следующей командой:
```bash
git checkout -b feature/35 main  # создаём ветку
```
Команда не только создаёт ветку feature/35 из последнего коммита ветки main, но и
сразу переключает репозиторий в отслеживание новой ветки feature/35. Добавив в конце
аргумент main мы явным образом указываем из какой ветки необходимо сделать ветку
feature/35.

В задании с добавлением своего ФИО изменения необходимо проводить в уже существующей
ветке "print_students".

После добавления коммитов опубликовать ветку feature/35 с её коммитами можно командой:
```bash
git push origin feature/35
```
При необходимости внесения изменений, сделать новые коммиты и опубликовать их той же
командой.

Когда задание выполнено, необходимо создать запрос на слияние (merge request) из своей
ветки.

Такая организация работы позволяет не смешивать работу по разным задачам и
автоматическая система проверки домашних заданий ориентирована именно на этот подход в
работе. При невыполнении

## 2. В merge request добавлять соответствующий комментарий
В
[комментарии](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
на запрос слияния (merge request), указать
```
Closes #35
где вместо 35 поставить номер своего issue
```
Так после подтверждения merge request (запрос на вливание) код окажется в основной
ветке, а issue (задача) будет закрыт автоматически. Выполнение ДЗ учитывается именно по
признаку того, что issue находится в состоянии closed.

## 3. Соблюдать стандартны качества кода
1. В функциях должны быть указаны типы аргументов и возвращаемых значений.
 Т.е. вместо
```python
def my_func(a, b):
    return a + b
```
Должно быть, например (если передаём вещественные значения):
```python
def my_func(a: float, b: float) -> float:
    return a + b
```

2. У функций должны быть добавлены doc-string, для этого удобно использовать расширение
 VSCode
[autodoctring](https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring),
которое делает большую часть работы. В этом проекте данное расширение добавлено в набор по
умолчанию. См. гиф анимацию по работе расширения:

<img src="https://github.com/NilsJPWerner/autoDocstring/raw/HEAD/images/demo.gif" alt="autodoctring" width="600"/>

3. Строки комментариев должны укладываться в длину строки 88 символов (первая вертикальная
   линия). Удобно использовать добавленное в умолчания для проекта расширение
   [rewrap](https://marketplace.visualstudio.com/items?itemName=stkb.rewrap)
   Т.е. вместо:
```python
def my_func(a: float, b: float) -> float:
    """Вычисляет много всего. Делает это замечательно, великолепно. Только документация большая, коротко не описать без потери смысла."""
```
   должно быть:
```python
def my_func(a: float, b: float) -> float:
    """Вычисляет много всего. Делает это замечательно, великолепно.
    Только документация большая, коротко не описать без потери смысла.
    """
```

4. Код должен быть отформатирован с помощью
[Black](https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter),
в vscode выполняется автоматически при сохранении файла Python. В файле настроек
.vscode/settings.json этому соответствуют следующие строки
```json
{
    "[python]": {
        "editor.formatOnSave": true,
        "editor.defaultFormatter": "ms-python.black-formatter",
    },
}
```

5. В коде должны быть удалены неиспользуемые импорты библиотек, используемые импорты
должны быть структурированы. Выполняется автоматически при сохранении расширением
[Ruff](https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff). В файле
настроек .vscode/settings.json этому соответствуют следующие строки
```json
{
    "[python]": {
        "editor.formatOnSave": true,
        "editor.codeActionsOnSave": {
            "source.organizeImports": true,
            "source.fixAll": true,
        },
    },
}
```
