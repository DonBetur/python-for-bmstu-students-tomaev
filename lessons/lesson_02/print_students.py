# Формируем словарь, в котором ключ (key) - это номер группы, а значение (value) -
# список студентов группы
students = {
    "АК1-111": [
        "Иванов Иван Иваныч",
        "Кузьмин Кузьма Кузьмич",
        "Петров Пётр Петрович",
    ],
    "АК2-111": [
        "Иванов Иван Иваныч",
        "Кузьмин Кузьма Кузьмич",
        "Петров Пётр Петрович",
    ],
    "АК2-112": [
	"Томаев Иван Ибрагимович"],
}

# for key, value in dict.items()
for group_name, group_students in students.items():
    print(group_name)
    for student_name in group_students:
        print(student_name)
